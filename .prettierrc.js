module.exports = {
  "bracketSpacing": false,
  "singleQuote": true,
  "trailingComma": 'es5',
  "editor.formatOnSave": true,
};
