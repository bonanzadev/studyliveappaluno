import React from 'react';
import { StatusBar } from 'react-native';
import Reactotron from 'reactotron-react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider } from 'react-redux';
import './config/ReactotronConfig'; //Precisa vir primeiro que a store
import { store, persistor } from './store';
import { DARK_THEME } from './styles/colors/';

import Home from './pages/Home';
import Login from './pages/Login';
import Cadastro from './pages/Cadastro';
import Esquecisenha from './pages/Esquecisenha';
import Conta from './pages/Conta';
import Plano from './pages/Plano';
import Perfil from './pages/Perfil';

const Stack = createStackNavigator();

console.tron = Reactotron
  .configure()
  .useReactNative()
  .connect()

function App({ innitialRoute }) {
  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={DARK_THEME.main} />
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <NavigationContainer>
            <Stack.Navigator
              initialRouteName={innitialRoute}
              screenOptions={
                {
                  headerShown: false,
                }
              }
            >
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="Cadastro" component={Cadastro} />
              <Stack.Screen name="Esquecisenha" component={Esquecisenha} />
              <Stack.Screen name="Home" component={Home} />
              <Stack.Screen name="Conta" component={Conta} />
              <Stack.Screen name="Plano" component={Plano} />
              <Stack.Screen name="Perfil" component={Perfil} />
            </Stack.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    </>
  );
}

export default App;
