/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  StyleSheet,
  Animated,
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
var {width} = Dimensions.get('window');

export default class BottomTab extends Component {
  state = {
    tabSelected: this.props.default,
    valueMoviment: new Animated.Value(0),
    opacity2: new Animated.Value(0),
    borderX: 8,
  }

  componentDidMount(){
    Animated.timing(this.state.opacity2, {
      toValue: 1,
      duration: 1000,
    }).start();
    this.moveTo();
  }

  moveTo = () => {
    Animated.spring(this.state.valueMoviment, {
      bounciness: 12,
      toValue: this.state.borderX,
    }).start();
  }

  selectTab1 = () => {
    this.setState({ tabSelected: 1, borderX: 8 }, () => {
      this.moveTo();
      this.props.navigation.push('Home');
    });
  }
  selectTab2 = () => {
    const pos = Math.round((width / 5) * 1) + 2;
    this.setState({ tabSelected: 2, borderX: pos }, () => {
      this.moveTo();
    });
  }
  selectTab3 = () => {
    const pos = Math.round((width / 5) * 2) + 2;
    this.setState({ tabSelected: 3, borderX: pos }, () => {
      this.moveTo();
      this.props.navigation.push('Plano');
    });
  }
  selectTab4 = () => {
    const pos = Math.round((width / 5) * 3) + 2;
    this.setState({ tabSelected: 4, borderX: pos }, () => {
      this.moveTo();
    });
  }
  selectTab5 = () => {
    const pos = Math.round(width * 0.80);
    this.setState({ tabSelected: 5, borderX: pos }, () => {
      this.moveTo();
      this.props.navigation.push('Conta');
    });
  }

  render() {
    const { tabSelected, valueMoviment } = this.state;
    return (
      <View style={styles.container}>
        <Animated.View style={[styles.barBorderSelected, {
          left: valueMoviment,
        }]}>
          {/*<Image source={require('../images/dark/button_tab.png')} resizeMode="contain" syle={{ width: '100%', height: '100%' }} />*/}
        </Animated.View>
        <View style={styles.bar}>
          { tabSelected === 1 &&
            <TouchableOpacity style={[styles.tabButton,{ paddingTop: 5 }]}>
              <Icon
                name="home"
                size={20}
                color="#FFF"
              />
              <Text style={styles.tabButtonLabel}>Início</Text>
            </TouchableOpacity>
          }
          { tabSelected !== 1 &&
            <TouchableOpacity style={styles.tabButton} onPress={this.selectTab1}>
              <Icon
                name="home"
                size={20}
                color="#FFF"
              />
            </TouchableOpacity>
          }
          { tabSelected === 2 &&
            <TouchableOpacity style={[styles.tabButton,{ paddingTop: 5 }]}>
              <Icon
                name="message-text-outline"
                size={20}
                color="#FFF"
              />
              <Text style={styles.tabButtonLabel}>Mensagens</Text>
            </TouchableOpacity>
          }
          { tabSelected !== 2 &&
            <TouchableOpacity style={styles.tabButton} onPress={this.selectTab2}>
              <Icon
                name="message-text-outline"
                size={20}
                color="#FFF"
              />
            </TouchableOpacity>
          }
          { tabSelected === 3 &&
            <TouchableOpacity style={[styles.tabButton,{ paddingTop: 5 }]}>
              <Icon
                name="diamond-stone"
                size={20}
                color="#FFF"
              />
              <Text style={styles.tabButtonLabel}>Planos</Text>
            </TouchableOpacity>
          }
          { tabSelected !== 3 &&
            <TouchableOpacity style={styles.tabButton} onPress={this.selectTab3}>
              <Icon
                name="diamond-stone"
                size={20}
                color="#FFF"
              />
            </TouchableOpacity>
          }
          { tabSelected === 4 &&
            <TouchableOpacity style={[styles.tabButton,{ paddingTop: 5 }]}>
              <Icon
                name="school"
                size={20}
                color="#FFF"
              />
              <Text style={styles.tabButtonLabel}>Histórico</Text>
            </TouchableOpacity>
          }
          { tabSelected !== 4 &&
            <TouchableOpacity style={styles.tabButton} onPress={this.selectTab4}>
              <Icon
                name="school"
                size={20}
                color="#FFF"
              />
            </TouchableOpacity>
          }
          { tabSelected === 5 &&
            <TouchableOpacity style={[styles.tabButton,{ paddingTop: 5 }]}>
              <Icon
                name="account-circle-outline"
                size={20}
                color="#FFF"
              />
              <Text style={styles.tabButtonLabel}>Conta</Text>
            </TouchableOpacity>
          }
          { tabSelected !== 5 &&
            <TouchableOpacity style={styles.tabButton} onPress={this.selectTab5}>
              <Icon
                name="account-circle-outline"
                size={20}
                color="#FFF"
              />
            </TouchableOpacity>
          }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
  },
  barBorderSelected: {
    width: 54,
    height: 10,
    position: 'absolute',
    zIndex: 21,
    bottom: 44,
    elevation: 3,
    left: '40%',
  },
  bar: {
    width: '100%',
    height: 42,
    backgroundColor: '#7159C1',
    position: 'absolute',
    zIndex: 20,
    borderTopWidth: 0,
    borderTopColor: '#e9e9e9',
    bottom: 0,
    shadowOpacity: 0.8,
    shadowRadius: 20,
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: {
        height: 2,
        width: 2,
    },
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  tabButton: {
    width: '20%',
    alignItems: 'center',
  },
  tabButtonLabel: {
    fontSize: 10,
    color: '#FFF',
    marginBottom: 5,
  },
});
