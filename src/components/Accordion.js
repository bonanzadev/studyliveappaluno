/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Home extends Component {
  state = {
    sections: this.props.sections,
    activeSection: this.props.activeSection,
    theme: this.props.theme,
    style: this.props.style,
  }

  setActiveAccordion = (item) => {
    this.setState({
      activeSection: item.id === this.state.activeSection ? 0 : item.id,
    });
  }

  renderActiveAccordion = (item) => {
    return (
      <View style={this.state.style.accordionContent}>
        <Text style={this.state.style.accordionLabel}>
          { item.children }
        </Text>
      </View>
    );
  }

  render() {
    const { theme, activeSection } = this.state;
    return (
      this.state.sections.map((item) => (
        <View key={item.id} style={{ width: '100%' }}>
          <TouchableOpacity style={this.state.style.accordionItem} onPress={() => this.setActiveAccordion(item)}>
            <Icon
              name={item.id === activeSection ? "chevron-up" : "chevron-down"}
              size={26}
              color={theme === 'DARK' ? '#FFF' : '#555'}
            />
            <Text style={this.state.style.accordionLabel}>{ item.title }</Text>
          </TouchableOpacity>
          { item.id === activeSection && this.renderActiveAccordion(item) }
        </View>
      ))
    );
  }
}
