const Fonts = {
  Light: 'Montserrat-Light',
  Bold: 'Montserrat-Bold',
  Italic: 'Montserrat-Italic',
  Regular: 'Montserrat-Regular',
  Medium: 'Montserrat-Medium',
};

export default Fonts;
