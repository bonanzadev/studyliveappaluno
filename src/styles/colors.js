export const DARK_THEME = {
  main: '#7159C1',
  bg1: '#1D1C24' /** */,
  bg2: '#27262C' /** Box background */,
  bg3: '#222128' /** Box background */,
  border1: '#333' /** Borders */,
  border2: '#292929' /** Borders */,
  shadow: '#000',
  txt1: '#777',
  txt2: '#999',
  txt3: '#FFF',
  txt4: '#FFF',
};

export const WHITE_THEME = {
  main: '#7159C1',
  bg1: '#FFF' /** */,
  bg2: '#F5F6FA' /** Box background */,
  bg3: '#f5f6F9' /** Box background */,
  border1: '#DDE4FD' /** Borders */,
  border2: '#f0f0f0' /** Borders */,
  border3: '#e9e9e9' /** Borders */,
  shadow: '#e9e9e9',
  txt1: '#AAA',
  txt2: '#CCC',
  txt3: '#777',
  txt4: '#FFF',
};
