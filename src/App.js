import React from 'react';
import Routes from './routes';

import { store } from './store';

const { signed } = store.getState().auth;

export default function App() {
  return <Routes innitialRoute={signed ? 'Login' : 'Home'} />
}
