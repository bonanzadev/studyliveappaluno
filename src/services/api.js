import axios from 'axios';

const api = axios.create({
  baseURL: 'https://api.studylive.com.br/',
});

export default api;
