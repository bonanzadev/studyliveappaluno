import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose,
} from 'redux';

import { persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import Reactotron from '../config/ReactotronConfig';
import rootSaga from './rootSaga';
import auth from './modules/auth/reducer';
import user from './modules/user/reducer';

import persistReducers from './persistReducers';

const sagaMonitor = __DEV__
  ? console.tron.createSagaMonitor()
  : null;

const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

const middlewares = [sagaMiddleware];

const rootReducer = combineReducers({
  user,
  auth
});

const enhancer = __DEV__
  ? compose(
    console.tron.createEnhancer(),
    applyMiddleware(...middlewares)
  )
  : applyMiddleware(...middlewares);

const store = createStore(persistReducers(rootReducer), enhancer);
const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };
