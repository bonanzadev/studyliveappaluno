import { call, put, all, takeLatest } from 'redux-saga/effects';
import api from '../../../services/api';
import { editThemeSuccess } from './actions'

function* editTheme(action) {
  const response = yield call(api.post, `/updateTheme`, { theme: action.text });
  console.log(response.data);
  yield put(editThemeSuccess(response.data));
}

export default all([
  takeLatest('@user/THEME_REQUEST', editTheme),
])
