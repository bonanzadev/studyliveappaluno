import produce from 'immer';

const INITIAL_STATE = {
  id: '',
  apelido: '',
  nome: '',
  saldo: '',
  avatar: '',
  theme: 'DARK',
};

export default function user(state = INITIAL_STATE, action) {
  //console.log(action)
  action = (action === undefined) ? '' : action;
  switch (action.type) {
    case '@auth/SIGN_IN_SUCCESS':
      return produce(state, draft => {
        draft.id = action.payload.usuario.id;
      })
    case '@user/EDIT':
      return {
        id: action.state.id || state.id,
        nome: action.state.nome || state.nome,
        apelido: action.state.apelido || state.apelido,
        saldo: action.state.saldo || state.saldo,
        avatar: action.state.avatar || state.avatar,
        theme: action.state.theme || state.theme,
      }
      break;
    case '@user/SALDO':
      return {
        ...state,
        saldo: action.state.saldo || state.saldo,
      }
      break;
    case '@user/THEME':
      return {
        ...state,
        theme: action.text,
      }
      break;
    default:
      return state;
  }
}
