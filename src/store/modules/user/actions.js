export function editUser(text) {
  return {
    type: '@user/EDIT',
    text,
  }
}

export function editThemeRequest(text) {
  return {
    type: '@user/THEME_REQUEST',
    text,
  }
}

export function editThemeSuccess(text) {
  return {
    type: '@user/THEME_SUCCESS',
    text,
  }
}
