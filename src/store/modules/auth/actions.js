export function signInRequest(email, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: {
      email, password,
    }
  }
}

export function signInSuccess(token, usuario) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: {
      token, usuario,
    }
  }
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE',
  }
}
