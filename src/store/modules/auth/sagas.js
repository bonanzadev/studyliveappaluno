import { Alert } from 'react-native';
import { all, takeLatest, call, put } from 'redux-saga/effects';
import api from '../../../services/api';
import * as Nav from '../../../utils/Navigation';

import { signInSuccess, signFailure } from './actions';

export function* signIn({ payload }) {

  try {
    const { email, password } = payload;

    if (email === '' || password === '') {
      Alert.alert('Falha na autenticação', 'Preencha os campos corretamente');
      return false;
    }

    const response = yield call(api.post, 'sessions', {
      email, senha: password, tipo_usuario: 'aluno'
    });

    const { token, usuario } = response.data;

    Nav.navigate('Home');

    yield put(signInSuccess(token, usuario));

  } catch (err) {
    console.tron.log(err)
    Alert.alert('Falha na autenticação', err.error);
    yield put(signFailure());
  }
}

export default all([
  takeLatest('@auth/SIGN_IN_REQUEST', signIn)
]);
