import { all } from 'redux-saga/effects';

import user from './modules/user/sagas';
import auth from './modules/auth/sagas';

export default function* rootSaga() {
  return yield all([user, auth]);
}
