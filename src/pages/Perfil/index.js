import React, { Component } from 'react';
import {
  Platform,
  Animated, Easing,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import YouTube from 'react-native-youtube';
import Swiper from 'react-native-swiper';
import Avatar from '../../components/Avatar';
import Accordion from '../../components/Accordion';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../store/modules/user/actions';

import DARK_ANDROID from './styles/dark.android';
import WHITE_ANDROID from './styles/white.android';

class Perfil extends Component {

  constructor(props){
    super(props);

    this.state = {
      theme: this.props.user.theme,
      offset: new Animated.ValueXY({ x:-50, y: 100 }),
      opacity2: new Animated.Value(0),
      tabSel: 0,
      activeSection: 0,
      sections: [
        { id: 1, title: 'Matematica', children: 'açsoidj oaiçsdoj , auihdauis aishd ialsd liahid' },
        { id: 2, title: 'Portugues', children: 'açsoidj oaiçsdoj , auihdauis aishd ialsd liahid' },
      ],
      dias: [
        { num: 1, title: 'Ter' },
        { num: 2, title: 'Qua' },
        { num: 3, title: 'Qui' },
        { num: 4, title: 'Sex' },
        { num: 5, title: 'Sab' },
        { num: 6, title: 'Dom' },
      ],
      diaIndexActive: 0,
      horarios: [
        { id: 1, title: '00:00', disponivel: 1, },
        { id: 2, title: '00:15', disponivel: 1, },
        { id: 3, title: '00:30', disponivel: 1, },
        { id: 4, title: '00:45', disponivel: 1, },
        { id: 5, title: '01:00', disponivel: 0, },
        { id: 6, title: '01:15', disponivel: 0, },
        { id: 7, title: '01:30', disponivel: 1, },
        { id: 8, title: '01:45', disponivel: 1, },
      ],
      horariosSel: [],
    }

    this.startTheme();
  }

  startTheme = () => {

    if (Platform.OS === 'android') {
      this.style = this.state.theme === 'DARK' ? DARK_ANDROID : WHITE_ANDROID;
    } else {
      //this.style = this.state.theme === 'DARK' ? DARK_IOS : WHITE_IOS;
    }
  }

  toggleTheme = () => {

    this.setState({
      theme: this.state.theme === 'DARK' ? 'WHITE' : 'DARK'
    }, () => {
      console.log('c: ',this.state.theme,this.props.user.theme);
      this.props.editTheme(this.state.theme);
      this.startTheme();
    });
  }

  Perfil = () => {
    const { theme } = this.state;

    return (
      <View style={{ width: '100%', height: '100%' }}>
        <View style={this.style.insideContainer}>
          <View style={this.style.topo1}>
            <Avatar
              style={this.style.agendamentoAvatar}
              rounded
              size="medium"
              source={require('../../images/geral/avatar_model2.jpg')}
              activeOpacity={1}
            />
            <View style={this.style.topo1TextBox}>
              <Text style={this.style.topo1Title}>Isadora Gomes</Text>
              <View style={this.style.topo1Bar}>
                <View>
                  <Text style={this.style.topo1Label}>Avaliação</Text>
                  <Text style={this.style.topo1Value}>
                    <Icon
                      name="star"
                      size={16}
                      color={'gold'}
                    />
                    4.8
                  </Text>
                </View>
                <View>
                  <Text style={this.style.topo1Label}>Aulas</Text>
                  <Text style={this.style.topo1Value}>200</Text>
                </View>
                <View>
                  <Icon
                    name="message-text-outline"
                    size={22}
                    color={theme === 'DARK' ? '#FFF' : '#555'}
                  />
                </View>
                <View>
                  <Icon
                    name="heart-outline"
                    size={22}
                    color={theme === 'DARK' ? '#FFF' : '#555'}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={this.style.middle}>
            <Text style={this.style.mainTitle}>Apresentação</Text>
            <Text style={this.style.txt}>
              Lorem ipsum, or lipsum as it is sometimes known, is dummy
              text used in laying out print, graphic or web designs.
              The passage is attributed text used in laying out print, graphic or web designs.
            </Text>
            <View style={this.style.separador} />
            <Text style={this.style.mainTitle}>Currículo</Text>
            <Text style={this.style.txt}>
              Lorem ipsum, or lipsum as it is sometimes known, is dummy
              text used in laying out print, graphic or web designs.
            </Text>
            <View style={this.style.separador} />
            <Text style={this.style.mainTitle}>Disciplinas</Text>

            <Accordion
              sections={this.state.sections}
              activeSection={this.state.sections}
              theme={theme}
              style={this.style}
            />

          </View>
        </View>
      </View>
    );
  }

  setActiveAccordion = (item) => {
    this.setState({
      activeSection: item.id === this.state.activeSection ? 0 : item.id
    });
  }

  dateMoveLeft = () => {
    const { diaIndexActive } = this.state;
    if (diaIndexActive > 0) {
      this.setState({ diaIndexActive: (this.state.diaIndexActive - 1) });
    }
  }

  dateMoveRight = () => {
    const { diaIndexActive, dias } = this.state;
    if (diaIndexActive < (dias.length - 1)) {
      this.setState({ diaIndexActive: (this.state.diaIndexActive + 1) });
    }
  }

  Agendar = () => {
    const { theme, dias, diaIndexActive, horarios, horariosSel } = this.state;
    return (
      <View style={{ width: '100%', paddingLeft: 10, paddingRight: 10, }}>
        <View style={this.style.middle}>
          <Text style={this.style.mainTitle}>Disponibilidade</Text>
          <Text style={this.style.txt}>
            Escolha o dia e horário da sua aula.
          </Text>
          <Text style={this.style.txt}>
            Você receberá um aviso quando o professor confirmar seu agendamento.
          </Text>
          <View style={this.style.dateBox}>
            <TouchableOpacity onPress={this.dateMoveLeft}>
              <Icon
                name="chevron-left"
                size={26}
                color={theme === 'DARK' ? '#FFF' : '#555'}
              />
            </TouchableOpacity>
            { dias.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={item.num}
                    style={index === diaIndexActive ? this.style.dateBoxSel : this.style.dateBoxDia}
                    onPress={() => this.setState({ diaIndexActive: index }) }
                  >
                    <Text style={index === diaIndexActive ? this.style.diaLabelSel : this.style.diaLabel}>
                      { item.title }
                    </Text>
                    <Text style={index === diaIndexActive ? this.style.diaLabelSel : this.style.diaLabel}>
                      { item.num }
                    </Text>
                  </TouchableOpacity>
                );
            })}
            <TouchableOpacity onPress={this.dateMoveRight}>
              <Icon
                name="chevron-right"
                size={26}
                color={theme === 'DARK' ? '#FFF' : '#555'}
              />
            </TouchableOpacity>
          </View>
          <View style={this.style.timeBox}>
            { horarios.map((item) => {
                return (
                  <TouchableOpacity
                    key={item.id}
                    style={item.disponivel === 1 ? this.style.time : this.style.timeDisabled}
                  >
                    <Text style={this.style.timeLabel}>
                      { item.disponivel === 1 ? item.title : 'reservado' }
                    </Text>
                  </TouchableOpacity>
                );
            })}

          </View>
        </View>
      </View>
    );
  }

  tabSwipe = (index) => {
    this.setState({ tabSel: index })
  }

  tabSwipeLeft = () => {
    this.refs.swiper.scrollBy(-1, true);
  }

  tabSwipeRight = () => {
    this.refs.swiper.scrollBy(1, true);
  }

  render() {
    const { theme, tabSel } = this.state;
    return (
      <View style={this.style.container}>
        <ScrollView>
          <YouTube
            apiKey="studyliveapp"
            videoId="9wGzSaz_u3U" // The YouTube video ID
            play={false}
            controls={2}
            rel={false}
            loop={false}
            onReady={e => this.setState({ isReady: true })}
            onChangeState={e => this.setState({ status: e.state })}
            onChangeQuality={e => this.setState({ quality: e.quality })}
            onError={e => this.setState({ error: e.error })}
            style={{ alignSelf: 'stretch', height: 200 }}
          />
          <View style={this.style.tabs}>
            <TouchableOpacity style={this.state.tabSel === 0 ? this.style.tabSel : this.style.tab} onPress={this.tabSwipeLeft}>
              <Text style={this.state.tabSel === 0 ? this.style.tabLabelSel : this.style.tabLabel}>
                Perfil
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={this.state.tabSel === 1 ? this.style.tabSelRight : this.style.tab} onPress={this.tabSwipeRight}>
              <Text style={this.state.tabSel === 1 ? this.style.tabLabelSel : this.style.tabLabel}>
                Agendar
              </Text>
            </TouchableOpacity>
          </View>
          <Swiper
            ref="swiper"
            index={tabSel}
            showsButtons={false}
            bounces={true}
            showsPagination={false}
            scrollsToTop={true}
            onIndexChanged={(index) =>
              this.tabSwipe(index)
            }
          >
            { this.Perfil() }
            { this.Agendar() }
          </Swiper>
          <View style={this.style.emptySpace} />
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Perfil);

