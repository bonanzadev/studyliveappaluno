/* eslint-disable prettier/prettier */
import { StyleSheet, Dimensions } from 'react-native';
import Fonts from '../../../styles/fonts';
import { DARK_THEME } from '../../../styles/colors';

var { height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
    backgroundColor: DARK_THEME.bg1,
  },
  mainScrollView: {
    width: '100%',
    marginTop: 10,
    height: height - 70,
  },
  mainScrollViewInside: {
    paddingTop: 270,
  },
  topo: {
    width: '100%',
    position: 'absolute',
    zIndex: 10,
    height: 328,
  },
  insideContainer: {
    paddingTop: 25,
    paddingLeft: 15,
    paddingRight: 15,
  },
  topo1: {
    flexDirection: 'row',
    paddingLeft: 5,
  },
  hello: {
    fontFamily: Fonts.Light,
    fontSize: 14,
    color: 'white',
    marginLeft: 20,
    marginTop: 5,
  },
  notifications: {
    position: 'absolute',
    zIndex: 10,
    right: 0,
  },
  searchBox: {
    width: '100%',
    borderRadius: 42,
    backgroundColor: DARK_THEME.bg2,
    borderWidth: 1,
    borderColor: DARK_THEME.border1,
    marginTop: 30,
    flexDirection: 'row',
    paddingLeft: 10,
    shadowOpacity: 0.8,
    shadowRadius: 20,
    elevation: 5,
    shadowColor: DARK_THEME.shadow,
    shadowOffset: {
        height: 2,
        width: 2,
    },
  },
  searchIcon: {
    marginTop: 12,
    marginRight: 7,
    color: DARK_THEME.txt1,
  },
  searchInput: {
    width: '100%',
    fontFamily: Fonts.Light,
    fontSize: 13,
    color: DARK_THEME.txt1,
  },
  disciplinasBox: {
    marginTop: 15,
  },
  disciplinaBox: {
    width: 70,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disciplinaImg: {
    width: 46,
    height: 46,
  },
  mainTitle: {
    fontFamily: Fonts.Bold,
    fontSize: 18,
    color: DARK_THEME.main,
  },
  disciplinaLabel: {
    fontFamily: Fonts.Medium,
    fontSize: 10,
    color: DARK_THEME.txt2,
    marginTop: 5,
  },
  middle: {
    width: '100%',
    position: 'absolute',
    zIndex: 19,
  },
  agendamentosBox: {
    backgroundColor: DARK_THEME.bg2,
    borderColor: DARK_THEME.border1,
    borderWidth: 1,
    width: '100%',
    minHeight: 70,
    borderRadius: 10,
    marginTop: 5,
    marginBottom: 10,
  },
  topTextBoxAgendamento: {
    width: '80%', paddingLeft: 12, fontFamily: Fonts.Medium,
  },
  middleTextAgendamento: {
    padding: 10, paddingTop: 0,
  },
  agendamentoAvatar: {
    margin: 10,
  },
  agendamentoTopo: {
    flexDirection: 'row',
    padding: 7,
    paddingBottom: 5,
  },
  agendamentoTitle: {
    fontFamily: Fonts.Regular,
    fontSize: 16,
    color: DARK_THEME.txt2,
  },
  agendamentoLabel: {
    fontFamily: Fonts.Regular,
    fontSize: 12,
    color: DARK_THEME.txt3,
    marginTop: 3,
  },
  agendamentoDisplay: {
    fontFamily: Fonts.Regular,
    fontSize: 12,
    color: DARK_THEME.txt2,
  },
  standardButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 8,
    backgroundColor: DARK_THEME.main,
    width: '100%',
    height: 32,
  },
  standardButtonTxt: {
    fontFamily: Fonts.Regular,
    fontSize: 12,
    color: DARK_THEME.txt3,
  },
  emptySpace: {
    width: '100%',
    height: 200,
  },
});
