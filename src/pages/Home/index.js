import React, { Component } from 'react';
import {
  Platform,
  Animated, Easing,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BottomTab from '../../components/BottomTab';
import Avatar from '../../components/Avatar';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../store/modules/user/actions';

import DARK_ANDROID from './styles/dark.android';
import WHITE_ANDROID from './styles/white.android';
import DARK_IOS from './styles/dark.ios';
import WHITE_IOS from './styles/white.ios';

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      theme: this.props.user.theme,
      offset: new Animated.ValueXY({ x: 50, y: 100 }),
      scrollOffset: new Animated.Value(0),
      opacity2: new Animated.Value(0),
      arrOpenedAgendamento: [],
    }

    this.startTheme();
  }

  componentDidMount() {
    setTimeout(() => {
      this.showStage();
    }, 200);
  }

  startTheme = () => {

    if (Platform.OS === 'android') {
      this.style = this.state.theme === 'DARK' ? DARK_ANDROID : WHITE_ANDROID;
    } else {
      this.style = this.state.theme === 'DARK' ? DARK_IOS : WHITE_IOS;
    }
  }

  toggleTheme = () => {

    this.setState({
      theme: this.state.theme === 'DARK' ? 'WHITE' : 'DARK'
    }, () => {
      console.log('c: ', this.state.theme, this.props.user.theme);
      this.props.editTheme(this.state.theme);
      this.startTheme();
    });
  }

  showStage = () => {
    Animated.parallel([
      Animated.timing(this.state.opacity2, {
        toValue: 1,
        duration: 500,
      }),

      Animated.spring(this.state.offset.x, {
        toValue: 0,
        speed: 5,
        bounciness: 12,
        easing: Easing.elastic(0.7),
      }),

      Animated.spring(this.state.offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 12,
        easing: Easing.elastic(0.7),
      }),
    ], { useNativeDriver: true }).start();
  }

  render() {
    const { theme } = this.state;
    const img = theme === 'DARK' ? require('../../images/dark/curve.png') : require('../../images/white/curve2.png');
    const matematica = theme === 'DARK' ? require(`../../images/dark/icon_matematica.png`) : require('../../images/white/icon_matematica.png');
    const portugues = theme === 'DARK' ? require(`../../images/dark/icon_portugues.png`) : require('../../images/white/icon_portugues.png');
    const quimica = theme === 'DARK' ? require(`../../images/dark/icon_quimica.png`) : require('../../images/white/icon_quimica.png');
    const fisica = theme === 'DARK' ? require(`../../images/dark/icon_fisica.png`) : require('../../images/white/icon_fisica.png');
    const geografia = theme === 'DARK' ? require(`../../images/dark/icon_geografia.png`) : require('../../images/white/icon_geografia.png');

    return (
      <View style={this.style.container}>

        <Animated.View style={[this.style.insideContainer,
        {
          opacity: this.state.opacity2,
          transform: [{ translateY: this.state.offset.y }],
        },
        ]}>
          <Animated.ScrollView
            style={this.style.mainScrollview}
            contentContainerStyle={this.style.mainScrollViewInside}
            bounces={false}
            overScrollMode="never"
            scrollEventThrottle={16}
            onScroll={
              Animated.event([
                {
                  nativeEvent: {
                    contentOffset: {
                      y: this.state.scrollOffset
                    }
                  }
                }
              ]
              )}
          >
            <View>
              <Text style={this.style.mainTitle}>Meus agendamentos</Text>
              <View style={this.style.agendamentosBox}>
                <View style={this.style.agendamentoTopo}>
                  <Avatar
                    style={this.style.agendamentoAvatar}
                    rounded
                    size="medium"
                    source={require('../../images/geral/avatar_model2.jpg')}
                    activeOpacity={1}
                  />
                  <View style={this.style.topTextBoxAgendamento}>
                    <Text style={this.style.agendamentoTitle}>Isadora Gomes</Text>
                    <Text style={this.style.agendamentoDisplay}>Hoje ás 18:00</Text>
                  </View>
                </View>
                <View style={this.style.middleTextAgendamento}>
                  <Text style={this.style.agendamentoLabel}>Disciplinas:</Text>
                  <Text style={this.style.agendamentoDisplay} numberOfLines={1}>Matemática, Física, Química</Text>

                  <Text style={this.style.agendamentoLabel}>Assuntos:</Text>
                  <Text style={this.style.agendamentoDisplay} numberOfLines={1}>Equação 1, Equação 2, Números complexos</Text>

                  <TouchableOpacity style={this.style.standardButton} onPress={() => this.props.navigation.navigate('Perfil')}>
                    <Text style={this.style.standardButtonTxt}>ENTRAR NA SALA</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={this.style.agendamentosBox}>
                <View style={this.style.agendamentoTopo}>
                  <Avatar
                    style={this.style.agendamentoAvatar}
                    rounded
                    size="medium"
                    source={require('../../images/geral/avatar_model2.jpg')}
                    activeOpacity={1}
                  />
                  <View style={this.style.topTextBoxAgendamento}>
                    <Text style={this.style.agendamentoTitle}>Isadora Gomes</Text>
                    <Text style={this.style.agendamentoDisplay}>Hoje ás 18:00</Text>
                  </View>
                </View>
                <View style={this.style.middleTextAgendamento}>
                  <Text style={this.style.agendamentoLabel}>Disciplinas:</Text>
                  <Text style={this.style.agendamentoDisplay} numberOfLines={1}>Matemática, Física, Química</Text>

                  <Text style={this.style.agendamentoLabel}>Assuntos:</Text>
                  <Text style={this.style.agendamentoDisplay} numberOfLines={1}>Equação 1, Equação 2, Números complexos</Text>
                </View>
              </View>
              <View style={this.style.agendamentosBox}>
                <View style={this.style.agendamentoTopo}>
                  <Avatar
                    style={this.style.agendamentoAvatar}
                    rounded
                    size="medium"
                    source={require('../../images/geral/avatar_model2.jpg')}
                    activeOpacity={1}
                  />
                  <View style={this.style.topTextBoxAgendamento}>
                    <Text style={this.style.agendamentoTitle}>Isadora Gomes</Text>
                    <Text style={this.style.agendamentoDisplay}>Hoje ás 18:00</Text>
                  </View>
                </View>
                <View style={this.style.middleTextAgendamento}>
                  <Text style={this.style.agendamentoLabel}>Disciplinas:</Text>
                  <Text style={this.style.agendamentoDisplay} numberOfLines={1}>Matemática, Física, Química</Text>

                  <Text style={this.style.agendamentoLabel}>Assuntos:</Text>
                  <Text style={this.style.agendamentoDisplay} numberOfLines={1}>Equação 1, Equação 2, Números complexos</Text>
                </View>
              </View>

              <View style={this.style.emptySpace} />

            </View>
          </Animated.ScrollView>
        </Animated.View>
        <BottomTab navigation={this.props.navigation} default={1} />
        <Animated.View style={[this.style.topo, {
          height: this.state.scrollOffset.interpolate({
            inputRange: [0, 328],
            outputRange: [328, 70],
            extrapolate: 'clamp',
            useNativeDriver: true,
          }),
          opacity: this.state.scrollOffset.interpolate({
            inputRange: [0, 100, 328],
            outputRange: [1, 0.1, 0],
            extrapolate: 'clamp',
            useNativeDriver: true,
          })
        }]}>
          <ImageBackground source={img} resizeMode="cover" style={{ width: '100%', height: '100%' }}>
            <View style={this.style.insideContainer}>
              <View style={this.style.topo1}>
                <Avatar
                  rounded
                  size="medium"
                  source={require('../../images/geral/mauricio.jpg')}
                  activeOpacity={1}
                />
                <Text style={this.style.hello}>Olá, Maurício Ferg</Text>
                <TouchableOpacity style={this.style.notifications}>
                  <Icon
                    name="bell-outline"
                    size={26}
                    color={theme === 'DARK' ? '#FFF' : '#555'}
                  />
                </TouchableOpacity>
              </View>
              <Animated.View style={[this.style.searchBox, {

                opacity: this.state.opacity2
              }]}>
                <Icon
                  style={this.style.searchIcon}
                  name="magnify"
                  size={26}
                  color="#FFFFFF"
                />
                <TextInput
                  type={'text'}
                  placeholder={'Qual assunto você procura?'}
                  placeholderTextColor={'#777'}
                  style={this.style.searchInput}
                />
              </Animated.View>

              <View style={this.style.disciplinasBox}>
                <Animated.View
                  style={[
                    {
                      transform: [
                        { translateX: this.state.offset.x },
                      ],
                      opacity: this.state.opacity2,
                    }
                  ]}
                >
                  <Text style={this.style.mainTitle}>Buscar por Disciplina</Text>
                  <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ flexDirection: 'row', marginTop: -5 }}>
                    <View style={this.style.disciplinaBox}>
                      <Image source={matematica} style={this.style.disciplinaImg} />
                      <Text style={this.style.disciplinaLabel}>Matemática</Text>
                    </View>
                    <View style={this.style.disciplinaBox}>
                      <Image source={portugues} style={this.style.disciplinaImg} />
                      <Text style={this.style.disciplinaLabel}>Português</Text>
                    </View>
                    <View style={this.style.disciplinaBox}>
                      <Image source={quimica} style={this.style.disciplinaImg} />
                      <Text style={this.style.disciplinaLabel}>Química</Text>
                    </View>
                    <View style={this.style.disciplinaBox}>
                      <Image source={fisica} style={this.style.disciplinaImg} />
                      <Text style={this.style.disciplinaLabel}>Física</Text>
                    </View>
                    <View style={this.style.disciplinaBox}>
                      <Image source={geografia} style={this.style.disciplinaImg} />
                      <Text style={this.style.disciplinaLabel}>Geografia</Text>
                    </View>
                  </ScrollView>
                </Animated.View>
              </View>
            </View>
          </ImageBackground>
        </Animated.View>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Home);

