/* eslint-disable prettier/prettier */
import { StyleSheet, Dimensions } from 'react-native';
import Fonts from '../../../styles/fonts';
import { WHITE_THEME } from '../../../styles/colors';

var { height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnVoltar: {
    position: 'absolute',
    zIndex: 10,
    top: 15,
    left: 15,
    width: 80,
    height: 80,
  },
  mainTitle: {
    fontFamily: Fonts.Bold,
    fontSize: 18,
    color: WHITE_THEME.main,
  },
  marketing: {
    fontFamily: Fonts.Medium,
    color: WHITE_THEME.txt2,
    fontSize: 14,
    marginTop: 10,
  },
  link: {
    fontFamily: Fonts.Bold,
    color: WHITE_THEME.main,
    fontSize: 14,
  },
  standardButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 52,
    marginTop: 8,
    backgroundColor: WHITE_THEME.main,
    width: '100%',
    height: 52,
  },
  standardButtonTxt: {
    fontFamily: Fonts.Regular,
    fontSize: 16,
    color: WHITE_THEME.txt2,
  },
  input: {
    width: '100%',
    height: 52,
    borderRadius: 52,
    backgroundColor: WHITE_THEME.bg1,
    borderColor: WHITE_THEME.border2,
    borderWidth: 1,
    color: WHITE_THEME.txt2,
    marginBottom: 10,
    paddingLeft: 20,
    fontFamily: Fonts.Medium,
    fontSize: 14,
  },
  circle: {
    height: 24,
    width: 24,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: WHITE_THEME.border1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    marginTop: 8,
  },
  checkedCircle: {
    width: 18,
    height: 18,
    borderRadius: 9,
    backgroundColor: WHITE_THEME.main,
  },
});
