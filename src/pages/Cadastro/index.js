/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  Platform,
  Animated, Easing,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import { TextInputMask } from 'react-native-masked-text';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../store/modules/user/actions';

import DARK_ANDROID from './styles/dark.android';
import WHITE_ANDROID from './styles/white.android';

class Cadastro extends Component {

  constructor(props){
    super(props);

    this.state = {
      theme: this.props.user.theme,
      nome: '',
      senha: '',
      repitasenha: '',
      email: '',
      data_nascimento: '',
      celular: '',
      aceite: false,
    }

    this.startTheme();
  }

  componentDidMount(){

  }

  handleNome = (nome) => { this.setState({ nome })};
  handleEmail = (email) => { this.setState({ email })};
  handleCelular = (celular) => { this.setState({ celular })};
  handleNascimento = (data_nascimento) => { this.setState({ data_nascimento })};
  handleSenha = (senha) => { this.setState({ senha })};
  handleRepita = (repitasenha) => { this.setState({ repitasenha })};

  startTheme = () => {

    if (Platform.OS === 'android') {
      this.style = this.state.theme === 'DARK' ? DARK_ANDROID : WHITE_ANDROID;
    } else {
      //this.style = this.state.theme === 'DARK' ? DARK_IOS : WHITE_IOS;
    }
  }

  handleSignup = () => {
    this.props.navigation.navigate('Login');
  }

  voltar = () => {
    this.props.navigation.navigate('Login');
  }

  renderCadastro = () => {
    const { theme } = this.state;
    return (
      <View style={{ flex: 1, width: '100%', justifyContent: 'center' }}>
        <TouchableOpacity style={this.style.btnVoltar} onPress={this.voltar}>
          <Icon
            name="ios-arrow-back"
            size={20}
            color={theme === 'DARK' ? '#FFF' : '#333'}
          />
        </TouchableOpacity>
        <View style={{ marginBottom: 10, marginLeft: 10, }}>
          <Text style={this.style.mainTitle}>Meu cadastro</Text>
        </View>
        <TextInput
          style={this.style.input}
          placeholderTextColor="#999"
          ref = {(input) => this.input1 = input}
          onSubmitEditing={() => this.input2.focus()}
          value={this.state.login}
          onChangeText={this.handleLogin}
          blurOnSubmit={false}
          autoCapitalize="none"
          returnKeyType="next"
          autoCorrect={false}
          placeholder="Qual seu nome?"
        />
        <TextInput
          style={this.style.input}
          placeholderTextColor="#999"
          keyboardType="email-address"
          ref = {(input) => this.input2 = input}
          onSubmitEditing={() => this.input3.focus()}
          value={this.state.login}
          onChangeText={this.handleLogin}
          blurOnSubmit={false}
          autoCapitalize="none"
          returnKeyType="next"
          autoCorrect={false}
          placeholder="Digite seu e-mail"
        />
        <TextInputMask
          style={this.style.input}
          refInput={(ref) => this.input3 = ref}
          onSubmitEditing={() => this.input4.focus()}
          onChangeText={this.handleCelular}
          value={this.state.celular}
          type={'cel-phone'}
          options={{
              maskType: 'BRL',
              withDDD: true,
              dddMask: '(99) '
          }}
          placeholderTextColor="#999"
          returnKeyType={'next'}
          placeholder="(DDD) Celular"
          underlineColorAndroid="transparent"
          blurOnSubmit={false}
        />
        <TextInputMask
          style={this.style.input}
          refInput={(ref) => this.input4 = ref}
          onSubmitEditing={() => this.input5.focus()}
          onChangeText={this.handleNascimento}
          type={'datetime'}
          options={{
            format: 'DD/MM/YYYY'
          }}
          value={this.state.data_nascimento}
          placeholderTextColor="#999"
          returnKeyType={'next'}
          placeholder="Data de Nascimento"
          underlineColorAndroid="transparent"
          blurOnSubmit={false}
        />
        <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

          <View style={{ width: '49%' }}>
            <TextInput
              style={this.style.input}
              secureTextEntry={true}
              placeholderTextColor="#999"
              ref = {(input) => this.input5 = input}
              onSubmitEditing={() => this.input6.focus()}
              value={this.state.senha}
              onChangeText={this.handleSenha}
              blurOnSubmit={false}
              autoCapitalize="none"
              returnKeyType="go"
              autoCorrect={false}
              placeholder="Senha"
            />
          </View>
          <View style={{ width: '49%' }}>
            <TextInput
              style={this.style.input}
              secureTextEntry={true}
              placeholderTextColor="#999"
              ref = {(input) => this.input6 = input}
              onSubmitEditing={() => this.handleSignup()}
              value={this.state.senha}
              onChangeText={this.handleRepita}
              blurOnSubmit={false}
              autoCapitalize="none"
              returnKeyType="go"
              autoCorrect={false}
              placeholder="Repita a Senha"
            />
          </View>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10, }}>
          <TouchableOpacity
            style={this.style.circle}
            onPress={() => this.setState({ aceite: !this.state.aceite })}
          >
            { this.state.aceite && (<View style={this.style.checkedCircle} />) }
          </TouchableOpacity>
          <Text style={this.style.marketing}>Eu li e aceito os
            <Text style={this.style.link}> Termos de uso</Text>
          </Text>
        </View>

        <TouchableOpacity style={this.style.standardButton} onPress={this.handleSignup}>
          <Text style={this.style.standardButtonTxt}>REGISTRAR</Text>
        </TouchableOpacity>

      </View>
    );
  }

  render() {
    const { theme } = this.state;

    if (theme === 'DARK') {
      var color1 = '#222';
      var color2 = '#191919';
      var color3 = '#111';
    } else {
      var color1 = '#FFF';
      var color2 = '#F5F6FA';
      var color3 = '#F5F6FA';
    }

    return (
      <LinearGradient start={{x: 0, y: 0}} end={{x: 2, y: 1}} colors={[color1, color2, color3]} style={this.style.container}>
        { this.renderCadastro() }
      </LinearGradient>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Cadastro);

