import { Dimensions } from 'react-native';
import Fonts from '../../styles/fonts';
import { DARK_THEME, WHITE_THEME } from '../../styles/colors';
import styled from 'styled-components/native';

var { height } = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
  width: 100%;
`;

export const Block = styled.View`
  flex: 2;
  justify-content: center;
  align-items: center;
`;

export const BoxLogo = styled.View`
  width: 181px;
  height: 110px;
`;

export const BoxWatchIcon = styled.View`
  width: 100%;
  height: 30px;
  justify-content: center;
  align-items: center;
`;

export const BoxMarketingText = styled.View`
  width: 100%;
  padding: 10px;
  align-items: center;
`;

export const Subtitle = styled.Text`
  font-size: 20px;
  font-family: '${Fonts.Bold}';
  color: ${DARK_THEME.main};
`;

export const Marketing = styled.Text`
  font-size: 14px;
  font-family: '${Fonts.Bold}';
  color: ${DARK_THEME.txt2};
`;

export const BoxPaginationMarketing = styled.View`
  width: 100%;
  padding: 10px;
  height: 30px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const Ball = styled.View`
  background-color: ${props => props.selected ? DARK_THEME.main : DARK_THEME.txt2};
  width: 10px;
  height: 10px;
  border-radius: 10px;
  margin-right: 5px;
`;

export const Submit = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-radius: 52px;
  margin-top: 8px;
  background-color: ${DARK_THEME.main};
  width: 100%;
  height: 52px;
`;

export const SubmitText = styled.Text`
  font-size: 16px;
  font-family: ${Fonts.Regular};
  color: ${DARK_THEME.txt3};
`;

export const Link = styled.Text`
  font-size: 14px;
  font-family: ${Fonts.Bold};
  color: ${DARK_THEME.txt3};
`;

export const BoxLink = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
`;


