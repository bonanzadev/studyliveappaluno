/* eslint-disable prettier/prettier */
import { StyleSheet, Dimensions } from 'react-native';
import Fonts from '../../../styles/fonts';
import { DARK_THEME } from '../../../styles/colors';

var { height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subtitle: {
    fontFamily: Fonts.Bold,
    color: DARK_THEME.main,
    fontSize: 16,
  },
  marketing: {
    fontFamily: Fonts.Medium,
    color: DARK_THEME.txt2,
    fontSize: 14,
    marginTop: 10,
  },
  link: {
    fontFamily: Fonts.Bold,
    color: DARK_THEME.txt3,
    fontSize: 14,
  },
  ball: {
    backgroundColor: DARK_THEME.txt2,
    width: 10,
    height: 10,
    borderRadius: 10,
    marginRight: 5,
  },
  ballSelected: {
    backgroundColor: DARK_THEME.main,
    width: 10,
    height: 10,
    borderRadius: 10,
    marginRight: 5,
  },
  standardButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 52,
    marginTop: 8,
    backgroundColor: DARK_THEME.main,
    width: '100%',
    height: 52,
  },
  standardButtonTxt: {
    fontFamily: Fonts.Regular,
    fontSize: 16,
    color: DARK_THEME.txt3,
  },
  input: {
    width: '100%',
    height: 52,
    borderRadius: 52,
    backgroundColor: DARK_THEME.bg2,
    borderColor: DARK_THEME.border1,
    borderWidth: 1,
    color: DARK_THEME.txt2,
    marginBottom: 10,
    paddingLeft: 20,
    fontFamily: Fonts.Medium,
    fontSize: 14,
  },
  boxEsqueci: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 10,
    marginTop: 10,
    width: '100%',
  },
  ou: {
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  ouBlock: {
    width: '50%',
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxSocial: {
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  socialButton: {
    marginRight: 15,
    marginLeft: 15,
  },
  bordaInf: {
    width: '30%',
    height: 1,
    backgroundColor: DARK_THEME.border2,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bordaInfBg: {
    width: 100,
    height: 10,
    backgroundColor: DARK_THEME.bg1,
    position: 'absolute',
    zIndex: 11,
  },
});
