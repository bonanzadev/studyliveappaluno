import React, { Component } from 'react';
import {
  Platform,
  Animated, Easing,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import ThemeProvider from 'styled-components';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as authActions from '../../store/modules/auth/actions';
import * as userActions from '../../store/modules/user/actions';

import DARK_ANDROID from './styles/dark.android';
import WHITE_ANDROID from './styles/white.android';

import { Container, Block, BoxLogo, BoxWatchIcon, BoxMarketingText, Subtitle, Marketing, BoxPaginationMarketing, Ball, Submit, SubmitText, Link, BoxLink } from './styles';

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      theme: this.props.user.theme,
      offset: new Animated.ValueXY({ x: 50, y: 100 }),
      scrollOffset: new Animated.Value(0),
      opacity2: new Animated.Value(0),
      arrOpenedAgendamento: [],
      act: 'inicio',
      login: '',
      senha: '',
    }
    this.startTheme();
  }

  handleLogin = (login) => { this.setState({ login }) };
  handleSenha = (senha) => { this.setState({ senha }) };

  startTheme = () => {

    if (Platform.OS === 'android') {
      this.style = this.state.theme === 'DARK' ? DARK_ANDROID : WHITE_ANDROID;
    } else {
      //this.style = this.state.theme === 'DARK' ? DARK_IOS : WHITE_IOS;
    }
  }

  toggleTheme = () => {

    this.setState({
      theme: this.state.theme === 'DARK' ? 'WHITE' : 'DARK'
    }, () => {
      console.log('c: ', this.state.theme, this.props.user.theme);
      this.props.editTheme(this.state.theme);
      this.startTheme();
    });
  }

  setAction = (act) => {
    this.setState({ act });
  }

  handleSubmit = () => {
    const { login, senha } = this.state
    this.props.signInRequest(login, senha);
    //this.props.navigation.navigate('Home');
  }

  register = () => {
    this.props.navigation.navigate('Cadastro');
  }

  recuperar = () => {
    this.props.navigation.navigate('Esquecisenha');
  }

  renderInicio = () => {
    const { theme } = this.state;

    const img = theme === 'DARK' ? require('../../images/dark/logo1.png') : require('../../images/white/logo1.png');
    return (
      <Container>
        <Block>
          <BoxLogo>
            <Image
              source={img}
              resizeMode="contain"
              style={{ width: '100%', height: '100%' }}
            />
          </BoxLogo>
        </Block>
        <Block>
          <BoxWatchIcon>
            <Icon
              name="clock-o"
              size={36}
              color={'#AAA'}
            />
          </BoxWatchIcon>
          <BoxMarketingText>
            <Subtitle>Make yout time!</Subtitle>
            <Marketing>But I must explain to you how all this
    mistaken idea of denouncing pleasure and
    praising pain was born and I will give you a
    complete account
            </Marketing>
          </BoxMarketingText>
          <BoxPaginationMarketing>
            <Ball selected />
            <Ball />
            <Ball />
          </BoxPaginationMarketing>
        </Block>
        <Block>
          <Submit onPress={this.register}>
            <SubmitText>REGISTRAR</SubmitText>
          </Submit>
          <BoxLink onPress={() => this.setAction('login')}>
            <Marketing>Já tenho conta</Marketing>
            <Link> Iniciar sessão</Link>
          </BoxLink>
        </Block>
      </Container>
    );
  }

  renderLogin = () => {
    const { theme } = this.state;
    const img = theme === 'DARK' ? require('../../images/dark/logo1.png') : require('../../images/white/logo1.png');

    return (
      <View style={{ flex: 1, width: '100%' }}>
        <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ width: 181, height: 110 }}>
            <Image
              source={img}
              resizeMode="contain"
              style={{ width: '100%', height: '100%' }}
            />
          </View>
        </View>
        <View style={{ flex: 4, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <TextInput
            style={this.style.input}
            placeholderTextColor="#AAA"
            keyboardType="email-address"
            ref={(input) => this.input1 = input}
            onSubmitEditing={() => this.input2.focus()}
            value={this.state.login}
            onChangeText={this.handleLogin}
            blurOnSubmit={true}
            autoCapitalize="none"
            returnKeyType="next"
            autoCorrect={false}
            placeholder="Digite seu e-mail"
          />
          <TextInput
            style={this.style.input}
            secureTextEntry={true}
            placeholderTextColor="#AAA"
            ref={(input) => this.input2 = input}
            onSubmitEditing={() => this.handleSubmit()}
            value={this.state.senha}
            onChangeText={this.handleSenha}
            blurOnSubmit={true}
            autoCapitalize="none"
            returnKeyType="go"
            autoCorrect={false}
            placeholder="Senha"
          />

          <TouchableOpacity style={this.style.standardButton} onPress={this.handleSubmit}>
            <Text style={this.style.standardButtonTxt}>ENTRAR</Text>
          </TouchableOpacity>

          <View style={this.style.ou}>
            <TouchableOpacity style={this.style.ouBlock} onPress={this.register}>
              <Text style={this.style.link}> Quero me cadastrar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={this.style.ouBlock} onPress={this.recuperar}>
              <Text style={this.style.link}> Recuperar senha</Text>
            </TouchableOpacity>
          </View>

        </View>
        <View style={{ flex: 2, width: '100%', justifyContent: 'center', alignItems: 'center' }}>

          <View style={this.style.ou}>


            <View style={this.style.bordaInf} />
            <Text style={this.style.marketing}>Continuar com</Text>
            <View style={this.style.bordaInf} />

          </View>
          <View style={this.style.boxSocial}>
            <TouchableOpacity style={this.style.socialButton}>
              <Icon
                name="facebook-f"
                size={20}
                color={theme === 'DARK' ? '#FFF' : '#AAA'}
              />
            </TouchableOpacity>
            <TouchableOpacity style={this.style.socialButton}>
              <Icon
                name="twitter"
                size={20}
                color={theme === 'DARK' ? '#FFF' : '#AAA'}
              />
            </TouchableOpacity>
            <TouchableOpacity style={this.style.socialButton}>
              <Icon
                name="google"
                size={20}
                color={theme === 'DARK' ? '#FFF' : '#AAA'}
              />
            </TouchableOpacity>
          </View>

        </View>
      </View>
    );
  }

  render() {
    const { theme, act } = this.state;

    if (theme === 'DARK') {
      var color1 = '#222';
      var color2 = '#191919';
      var color3 = '#111';
    } else {
      var color1 = '#FFF';
      var color2 = '#F5F6FA';
      var color3 = '#F5F6FA';
    }

    return (
      <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 2, y: 1 }} colors={[color1, color2, color3]} style={this.style.container}>
        {act === 'inicio' && this.renderInicio()}
        {act === 'login' && this.renderLogin()}
      </LinearGradient>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
  auth: state.auth,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(authActions, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Login);

