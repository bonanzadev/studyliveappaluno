/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  Platform,
  Animated, Easing,
  View,
  Text,
  Switch,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { CommonActions  } from '@react-navigation/native';
import BottomTab from '../../components/BottomTab';
import Avatar from '../../components/Avatar';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../store/modules/user/actions';

import DARK_ANDROID from './styles/dark.android';
import WHITE_ANDROID from './styles/white.android';

class Conta extends Component {

  constructor(props){
    super(props);

    this.state = {
      theme: this.props.user.theme,
      offset: new Animated.ValueXY({ x:-50, y: 100 }),
      opacity2: new Animated.Value(0),
      switchValue: false,
    }

    this.startTheme();
  }

  componentDidMount() {
    this.showStage()
  }

  startTheme = () => {

    if (Platform.OS === 'android') {
      this.style = this.state.theme === 'DARK' ? DARK_ANDROID : WHITE_ANDROID;
    } else {
      //this.style = this.state.theme === 'DARK' ? DARK_IOS : WHITE_IOS;
    }
  }

  toggleTheme = () => {

    this.setState({
      theme: this.state.theme === 'DARK' ? 'WHITE' : 'DARK'
    }, () => {
      console.log('c: ',this.state.theme,this.props.user.theme);
      this.props.editTheme(this.state.theme);
      this.startTheme();
    });
  }

  logout = () => {
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Login' },
          {
            name: 'Profile',
            params: { user: 'jane' },
          },
        ],
      })
    );
  }

  showStage = () => {
    Animated.parallel([
      Animated.timing(this.state.opacity2, {
        toValue: 1,
        duration: 500,
      }),
      Animated.spring(this.state.offset.x, {
        toValue: 0,
        speed: 5,
        bounciness: 12,
        easing: Easing.elastic(0.7),
      }),
      Animated.spring(this.state.offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 12,
        easing: Easing.elastic(0.7),
      }),
    ], { useNativeDriver: true }).start();
  }

  toggleSwitch = () => {
    const { theme, switchValue } = this.state;

    if (theme === 'DARK') {
      this.setState({ theme: 'WHITE', switchValue: !switchValue });
    } else {
      this.setState({ theme: 'DARK', switchValue: !switchValue });
    }

    this.toggleTheme();
  }

  render() {
    const { theme, switchValue } = this.state;

    return (
      <View style={this.style.container}>
        <Animated.View style={[this.style.topo1, {
          opacity: this.state.opacity2,
          transform: [{ translateX: this.state.offset.x }],
        }]}>
          <View style={{ marginTop: 40, }}>
            <Avatar
              rounded
              size="pluslarge"
              source={require('../../images/geral/mauricio.jpg')}
              activeOpacity={1}
            />
          </View>
          <View style={{ marginTop: 45, }}>
            <Text style={this.style.mainTitle}>Maurício Ferg</Text>
          </View>
          <View style={{ top: 20, position: 'absolute', zIndex: 10, right: 10, }}>
            <Switch
              onValueChange={this.toggleSwitch}
              value={switchValue}
            />
          </View>
        </Animated.View>
        <Animated.ScrollView
          style={[this.style.mainScrollview,
            {
              opacity: this.state.opacity2,
              transform: [{ translateY: this.state.offset.y }],
            },
          ]}
          contentContainerStyle={this.style.mainScrollViewInside}
        >
          <TouchableOpacity style={this.style.menuBox}>
            <Icon
              name="account-circle-outline"
              size={22}
              color={theme === 'DARK' ? '#FFF' : '#999'}
            />
            <Text style={this.style.menuLabel}>Minha conta</Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.style.menuBox}>
            <Icon
              name="teach"
              size={22}
              color={theme === 'DARK' ? '#FFF' : '#999'}
            />
            <Text style={this.style.menuLabel}>Professores favoritos</Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.style.menuBox}>
            <Icon
              name="share-variant"
              size={22}
              color={theme === 'DARK' ? '#FFF' : '#999'}
            />
            <Text style={this.style.menuLabel}>Indicar para meus amigos</Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.style.menuBox}>
            <Icon
              name="email"
              size={22}
              color={theme === 'DARK' ? '#FFF' : '#999'}
            />
            <Text style={this.style.menuLabel}>Fale conosco</Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.style.menuBox}>
            <Icon
              name="view-list"
              size={22}
              color={theme === 'DARK' ? '#FFF' : '#999'}
            />
            <Text style={this.style.menuLabel}>Extrato financeiro</Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.style.menuBox}>
            <Icon
              name="cart"
              size={22}
              color={theme === 'DARK' ? '#FFF' : '#999'}
            />
            <Text style={this.style.menuLabel}>Minutos avulsos</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[this.style.menuBox,{ borderBottomWidth: 0, }]} onPress={this.logout}>
            <Icon
              name="logout"
              size={22}
              color={theme === 'DARK' ? '#FFF' : '#999'}
            />
            <Text style={this.style.menuLabel}>Sair</Text>
          </TouchableOpacity>
          <View style={this.style.emptySpace} />
        </Animated.ScrollView >
        <BottomTab navigation={this.props.navigation} default={5}/>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Conta);

