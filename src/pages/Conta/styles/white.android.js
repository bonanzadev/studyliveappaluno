/* eslint-disable prettier/prettier */
import { StyleSheet, Dimensions } from 'react-native';
import Fonts from '../../../styles/fonts';
import { WHITE_THEME } from '../../../styles/colors';

var { height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
    backgroundColor: WHITE_THEME.bg2,
  },
  insideContainer: {
    paddingTop: 50,
    paddingLeft: 15,
    paddingRight: 15,
  },
  topo1: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 180,
    padding: 15,
  },
  mainTitle: {
    fontFamily: Fonts.Bold,
    fontSize: 18,
    color: WHITE_THEME.main,
  },
  switch: {
    color: WHITE_THEME.main,
  },
  mainScrollView: {
    width: '100%',
    marginTop: 10,
    height: height - 50,
  },
  mainScrollViewInside: {
    height: height - 180,
    backgroundColor: WHITE_THEME.bg1,
  },
  menuBox: {
    width: '100%',
    height: 56,
    flexDirection: 'row',
    paddingLeft: 20,
    borderBottomColor: WHITE_THEME.border2,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  menuLabel: {
    fontFamily: Fonts.Medium,
    fontSize: 12,
    color: WHITE_THEME.txt1,
    marginLeft: 20,
  },
  emptySpace: {
    width: '100%',
    height: 200,
  },
});
