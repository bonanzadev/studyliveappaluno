/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  Platform,
  Animated, Easing,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import { TextInputMask } from 'react-native-masked-text';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../store/modules/user/actions';

import DARK_ANDROID from './styles/dark.android';
import WHITE_ANDROID from './styles/white.android';

class Esquecisenha extends Component {

  constructor(props){
    super(props);

    this.state = {
      theme: this.props.user.theme,
      nome: '',
      senha: '',
      repitasenha: '',
      email: '',
      data_nascimento: '',
      celular: '',
      aceite: false,
    }

    this.startTheme();
  }

  componentDidMount(){

  }

  handleEmail = (email) => { this.setState({ email })};

  startTheme = () => {

    if (Platform.OS === 'android') {
      this.style = this.state.theme === 'DARK' ? DARK_ANDROID : WHITE_ANDROID;
    } else {
      //this.style = this.state.theme === 'DARK' ? DARK_IOS : WHITE_IOS;
    }
  }

  handleEsqueci = () => {
    this.props.navigation.navigate('Login');
  }

  voltar = () => {
    this.props.navigation.navigate('Login');
  }

  renderCadastro = () => {
    const { theme } = this.state;
    return (
      <View style={{ flex: 1, width: '100%', justifyContent: 'center' }}>
        <TouchableOpacity style={this.style.btnVoltar} onPress={this.voltar}>
          <Icon
            name="ios-arrow-back"
            size={20}
            color={theme === 'DARK' ? '#FFF' : '#333'}
          />
        </TouchableOpacity>
        <View style={{ marginBottom: 10, marginLeft: 10, }}>
          <Text style={this.style.mainTitle}>Recuperar senha</Text>
        </View>
        <TextInput
          style={this.style.input}
          placeholderTextColor="#999"
          keyboardType="email-address"
          ref = {(input) => this.input2 = input}
          onSubmitEditing={() => this.input3.focus()}
          value={this.state.login}
          onChangeText={this.handleLogin}
          blurOnSubmit={false}
          autoCapitalize="none"
          returnKeyType="next"
          autoCorrect={false}
          placeholder="Digite seu e-mail"
        />

        <TouchableOpacity style={this.style.standardButton} onPress={this.handleSignup}>
          <Text style={this.style.standardButtonTxt}>ENVIAR</Text>
        </TouchableOpacity>

      </View>
    );
  }

  render() {
    const { theme } = this.state;

    if (theme === 'DARK') {
      var color1 = '#222';
      var color2 = '#191919';
      var color3 = '#111';
    } else {
      var color1 = '#FFF';
      var color2 = '#F5F6FA';
      var color3 = '#F5F6FA';
    }

    return (
      <LinearGradient start={{x: 0, y: 0}} end={{x: 2, y: 1}} colors={[color1, color2, color3]} style={this.style.container}>
        { this.renderCadastro() }
      </LinearGradient>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Esquecisenha);

