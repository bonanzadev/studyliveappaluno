/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  Platform,
  Animated, Easing,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BottomTab from '../../components/BottomTab';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../store/modules/user/actions';

import DARK_ANDROID from './styles/dark.android';
import WHITE_ANDROID from './styles/white.android';

class Plano extends Component {

  constructor(props){
    super(props);

    this.state = {
      theme: this.props.user.theme,
      offset: new Animated.ValueXY({ x:-50, y: 100 }),
      opacity2: new Animated.Value(0),
    }

    this.startTheme();
  }

  startTheme = () => {

    if (Platform.OS === 'android') {
      this.style = this.state.theme === 'DARK' ? DARK_ANDROID : WHITE_ANDROID;
    } else {
      //this.style = this.state.theme === 'DARK' ? DARK_IOS : WHITE_IOS;
    }
  }

  toggleTheme = () => {

    this.setState({
      theme: this.state.theme === 'DARK' ? 'WHITE' : 'DARK'
    }, () => {
      console.log('c: ',this.state.theme,this.props.user.theme);
      this.props.editTheme(this.state.theme);
      this.startTheme();
    });
  }

  render() {
    const { theme } = this.state;

    return (
      <View style={this.style.container}>
        <ScrollView>
          <View style={this.style.insideContainer}>
            <Text style={this.style.mainTitle}>Aula Experimental</Text>
            <View style={this.style.planosBox}>
              <View style={this.style.desconto}>
                <Image source={require('../../images/geral/65_off.png')} resizeMode="contain" style={{ width: '100%', height: '100%' }} />
              </View>
              <View style={this.style.planosBoxLeft}>
                <Text style={this.style.pacoteLabel}>QUERO CONHECER</Text>
                <Text style={this.style.pacoteMinutos}>30 minutos</Text>
                <TouchableOpacity style={this.style.standardButton}>
                  <Text style={this.style.standardButtonTxt}>SELECIONAR</Text>
                </TouchableOpacity>
              </View>
              <View style={this.style.planosBoxRight}>
                <Text style={this.style.precoLabel}>Preço por minuto</Text>
                <Text style={this.style.precoMinuto}>R$ 0,50</Text>
                <Text style={[this.style.pacoteLabel, { marginTop: 10 }]}>Valor</Text>
                <Text style={this.style.pacoteMinutos}>R$ 14,99</Text>
              </View>
            </View>
            <Text style={this.style.mainTitle}>Planos de assinatura mensal</Text>
            <Text style={this.style.condicoes}>
            * O plano é renovado automaticamente após 30 dias.
            </Text>
            <Text style={this.style.condicoes}>
            * Minutos não acumulativos.
            </Text>
            <Text style={this.style.condicoes}>
            * Cancelamento gratuito.
            </Text>
            <View style={this.style.planosBox}>
              <View style={this.style.planosBoxLeft}>
                <Text style={this.style.pacoteLabel}>PLANO</Text>
                <Text style={this.style.pacoteMinutos}>100 minutos</Text>
                <TouchableOpacity style={this.style.standardButton}>
                  <Text style={this.style.standardButtonTxt}>SELECIONAR</Text>
                </TouchableOpacity>
              </View>
              <View style={this.style.planosBoxRight}>
                <Text style={this.style.precoLabel}>Preço por minuto</Text>
                <Text style={this.style.precoMinuto}>R$ 1,40</Text>
                <Text style={[this.style.pacoteLabel, { marginTop: 10 }]}>Valor</Text>
                <Text style={this.style.pacoteMinutos}>R$ 140,00</Text>
              </View>
            </View>
            <View style={this.style.planosBox}>
              <View style={this.style.desconto}>
                <Image source={require('../../images/geral/10_off.png')} resizeMode="contain" style={{ width: '100%', height: '100%' }} />
              </View>
              <View style={this.style.planosBoxLeft}>
                <Text style={this.style.pacoteLabel}>PLANO</Text>
                <Text style={this.style.pacoteMinutos}>300 minutos</Text>
                <TouchableOpacity style={this.style.standardButton}>
                  <Text style={this.style.standardButtonTxt}>SELECIONAR</Text>
                </TouchableOpacity>
              </View>
              <View style={this.style.planosBoxRight}>
                <Text style={this.style.precoLabel}>Preço por minuto</Text>
                <Text style={this.style.precoMinuto}>R$ 1,26</Text>
                <Text style={[this.style.pacoteLabel, { marginTop: 10 }]}>Valor</Text>
                <Text style={this.style.pacoteMinutos}>R$ 378,00</Text>
              </View>
            </View>
            <View style={this.style.planosBox}>
              <View style={this.style.desconto}>
                <Image source={require('../../images/geral/20_off.png')} resizeMode="contain" style={{ width: '100%', height: '100%' }} />
              </View>
              <View style={this.style.planosBoxLeft}>
                <Text style={this.style.pacoteLabel}>PLANO</Text>
                <Text style={this.style.pacoteMinutos}>500 minutos</Text>
                <TouchableOpacity style={this.style.standardButton}>
                  <Text style={this.style.standardButtonTxt}>SELECIONAR</Text>
                </TouchableOpacity>
              </View>
              <View style={this.style.planosBoxRight}>
                <Text style={this.style.precoLabel}>Preço por minuto</Text>
                <Text style={this.style.precoMinuto}>R$ 1,12</Text>
                <Text style={[this.style.pacoteLabel, { marginTop: 10 }]}>Valor</Text>
                <Text style={this.style.pacoteMinutos}>R$ 560,00</Text>
              </View>
            </View>
        </View>
        <View style={this.style.emptySpace} />
        </ScrollView>
        <BottomTab navigation={this.props.navigation} default={3}/>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);


export default connect(mapStateToProps,mapDispatchToProps)(Plano);

