/* eslint-disable prettier/prettier */
import { StyleSheet, Dimensions } from 'react-native';
import Fonts from '../../../styles/fonts';
import { WHITE_THEME } from '../../../styles/colors';

var { height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
    backgroundColor: WHITE_THEME.bg2,
  },
  insideContainer: {
    paddingTop: 30,
    paddingLeft: 15,
    paddingRight: 15,
  },
  mainTitle: {
    fontFamily: Fonts.Bold,
    fontSize: 18,
    color: WHITE_THEME.main,
  },
  mainScrollView: {
    width: '100%',
    height: height - 50,
  },
  mainScrollViewInside: {
    height: height,
  },
  emptySpace: {
    width: '100%',
    height: 200,
  },
  planosBox: {
    backgroundColor: WHITE_THEME.bg1,
    borderColor: WHITE_THEME.border2,
    borderWidth: 1,
    width: '100%',
    minHeight: 110,
    borderRadius: 10,
    marginTop: 5,
    marginBottom: 10,
    flexDirection: 'row',
    shadowOpacity: 0.8,
    shadowRadius: 20,
    elevation: 5,
    shadowColor: WHITE_THEME.shadow,
    shadowOffset: {
        height: 2,
        width: 2,
    },
  },
  planosBoxLeft: {
    width: '60%',
    justifyContent: 'center',
    alignItems:'flex-start',
    paddingLeft: '4%',
  },
  planosBoxRight: {
    width: '40%',
    justifyContent: 'center',
    alignItems:'flex-start',
    paddingLeft: '4%',
  },
  pacoteLabel: {
    fontFamily: Fonts.Regular,
    fontSize: 12,
    color: WHITE_THEME.txt1,
  },
  pacoteMinutos: {
    fontFamily: Fonts.Bold,
    fontSize: 18,
    color: WHITE_THEME.txt3,
  },
  standardButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: 8,
    backgroundColor: WHITE_THEME.main,
    width: '100%',
    height: 32,
  },
  standardButtonTxt: {
    fontFamily: Fonts.Regular,
    fontSize: 12,
    color: WHITE_THEME.txt4,
  },
  precoLabel: {
    fontFamily: Fonts.Regular,
    fontSize: 10,
    color: WHITE_THEME.txt1,
  },
  precoMinuto: {
    fontFamily: Fonts.Regular,
    fontSize: 12,
    color: WHITE_THEME.txt3,
  },
  condicoes: {
    fontFamily: Fonts.Regular,
    fontSize: 12,
    color: WHITE_THEME.txt3,
    marginBottom: 5,
  },
  desconto: {
    width: 48,
    height: 48,
    position: 'absolute',
    zIndex: 10,
    right: -15,
    top: -15,
  },
});
